const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const cors = require('cors');
const {v4: uuidv4} = require('uuid');

const app = express();
const server = http.createServer(app);
const io = socketio(server, {
    cors: {
        origin: '*',
        methods: ['GET', 'POST']
    }
});

// Port 3000でサーバーを起動
const PORT = process.env.PORT || 3000;
// ルームidを保持するためのオブジェクト
const rooms = new Set();
const usersByRoom = new Map();

io.on('connection', (socket) => {
    console.log('New client connected');

    socket.on('create-room', () => {
        //すでに部屋に参加している場合は、部屋を作成しない
        console.log('Rooms:', rooms);
        console.log('Socket rooms:', socket.rooms);
        if (getRoomIdsBySocketId(socket.id).length !== 0) {
            socket.emit('room-create-error', 'You are already in a room');
            return;
        }
        const roomId = generateRoomID();
        while (rooms.has(roomId)) {
            roomId = generateRoomID();
        }

        socket.join(roomId);
        socket.emit('room-created', roomId);
        console.log('Room created with ID:', roomId);
        rooms.add(roomId);
        // ルームにユーザーを追加
        const userId = uuidv4();
        usersByRoom.set(roomId, [{ id: userId, socketId: socket.id }]);
        console.log('Users in room:', usersByRoom.get(roomId));
    });

    socket.on('join-room', (roomId) => {
        console.log('getRoomIdsBySocketId(socket.id):', getRoomIdsBySocketId(socket.id));
        console.log(getRoomIdsBySocketId(socket.id).length !== 0);
        if (getRoomIdsBySocketId(socket.id).length !== 0) {
            console.log('You are already in a room');
            socket.emit('join-room-error', 'You are already in a room');
            return;
        }
        if (io.sockets.adapter.rooms.has(roomId)) {
            socket.join(roomId);
            socket.emit('room-joined', roomId);
            console.log('Client joined room with ID:', roomId);
            // ユーザーをルームに追加
            const userId = uuidv4();
            const users = usersByRoom.get(roomId) || [];
            users.push({ id: userId, socketId: socket.id });
            usersByRoom.set(roomId, users);
            io.to(roomId).emit('users-in-room', users);
            console.log('Users in room:', users);
        }else{
            socket.emit('room-not-found');
            console.log('Room not found with ID:', roomId);
        }
    });

    socket.on('leave-room', () => {
        rooms.forEach(roomId => {
            if (roomId !== socket.id) {
                socket.leave(roomId);
                const users = usersByRoom.get(roomId) || [];
                const index = users.findIndex(user => user.socketId === socket.id);
                if (index !== -1) {
                    users.splice(index, 1);
                    console.log('Users in room:', users.length)
                    if (users.length === 0) {
                        usersByRoom.delete(roomId); // ルームが空なら削除
                        rooms.delete(roomId);
                        console.log('Room deleted with ID:', roomId);
                    } else {
                        io.to(roomId).emit('users-in-room', users);
                    }
                }
            }
        });
        console.log('Client left room');
    });

    // ルーム一覧を送信
    socket.on('get-room-list', () => {
        //ルーム内のユーザー数を取得
        const roomList = [];
        rooms.forEach(roomId => {
            const users = usersByRoom.get(roomId) || [];
            roomList.push({ roomId, users: users.length });
        });
        socket.emit('room-list', roomList);
    });

    socket.on('disconnect', () => {
        console.log('Client disconnected');
        // ユーザーがルームから離脱した場合、ユーザーリストから削除
        usersByRoom.forEach((users, roomId) => {
            const index = users.findIndex(user => user.socketId === socket.id);
            if (index !== -1) {
                users.splice(index, 1);
                io.to(roomId).emit('users-in-room', users);
                console.log('Users in room:', users.length)
                if (users.length === 0) {
                    usersByRoom.delete(roomId); // ルームが空なら削除
                    rooms.delete(roomId); // ルームも削除
                    console.log('Room deleted with ID:', roomId);
                }
            }
        });
    });
});

server.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});

function generateRoomID() {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const roomIdLength = 4;
    let roomId = '';

    // ランダムな文字列を生成
    for (let i = 0; i < roomIdLength; i++) {
        const randomIndex = Math.floor(Math.random() * characters.length);
        roomId += characters.charAt(randomIndex);
    }

    return roomId;
}

function getRoomIdsBySocketId(socketId) {
    const roomIds = [];
    usersByRoom.forEach((users, roomId) => {
        const userIndex = users.findIndex(user => user.socketId === socketId);
        if (userIndex !== -1) {
            roomIds.push(roomId);
        }
    });
    return roomIds;
}