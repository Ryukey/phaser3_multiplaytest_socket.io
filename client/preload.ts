export class PreloadScenes extends Phaser.Scene {
    constructor() {
        super('preload');
    }
    preload() {

    }
    create() {
        this.add.text(100, 100, "Preload Scene", { fontSize: "32px", color: "#fff" });
        this.scene.start('lobby');
    }
}