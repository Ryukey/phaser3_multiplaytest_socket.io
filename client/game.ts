export class GameScene extends Phaser.Scene {
    constructor() {
        super("game");
    }
    create() {
        this.add.text(100, 100, "Game Scene", { fontSize: "32px", color: "#fff" });
        this.EndButton = this.add.text(100, 300, "End Game", {fontSize: "32px", color: "#fff"});
        this.EndButton.setInteractive();
        this.EndButton.on("pointerdown", () => {
            this.scene.start("result");
        });
    }
}