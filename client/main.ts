import Phaser from 'phaser';
import io from "socket.io-client";

import { PreloadScenes } from "./preload";
import { LobbyScene } from "./lobby";
import { GameScene } from "./game";
import { ResultScene } from "./result";

const socket = io("http://localhost:3000");

const config: Phaser.Types.Core.GameConfig = {
    type: Phaser.AUTO,
    scale: {
    mode: Phaser.Scale.FIT,
        width: 1920,
        height: 1080,
    },
    backgroundColor: '#028af8',
    parent: 'app',
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 0 },
        },
    },
    scene: [PreloadScenes, LobbyScene, GameScene, ResultScene],
};

const game = new Phaser.Game(config);
export { socket }