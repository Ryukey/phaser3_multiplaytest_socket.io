export class ResultScene extends Phaser.Scene {
    constructor() {
        super("result");
    }
    create() {
        this.add.text(100, 100, "Result Scene", { fontSize: "32px", color: "#fff" });
        this.BackLobbyButton = this.add.text(100, 300, "Back Lobby", {fontSize: "32px", color: "#fff"});
        this.BackLobbyButton.setInteractive();
        this.BackLobbyButton.on("pointerdown", () => {
            this.scene.start("lobby");
        });
    }
}