import { socket } from "./main";

export class LobbyScene extends Phaser.Scene {
    private roomIdText: Phaser.GameObjects.Text; // ルームIDを表示するテキスト
    private roomListTexts: Phaser.GameObjects.Text[] = []; // ルーム一覧のテキストを保持する配列
    private startButton: Phaser.GameObjects.Text;
    private createRoomButton: Phaser.GameObjects.Text;
    private joinRoomButton: Phaser.GameObjects.Text;
    private leaveRoomButton: Phaser.GameObjects.Text;

    constructor() {
    super("lobby");
    }

    create() {
        this.add.text(100, 100, "Lobby Scene", { fontSize: "32px", color: "#fff" });

        //ゲーム開始ボタン
        this.startButton = this.add.text(100, 300, "Start Game", {fontSize: "32px", color: "#fff"});
        this.startButton.setInteractive();
        this.startButton.on("pointerdown", () => {
            this.scene.start("game");
        });

        // ルーム作成ボタン
        this.createRoomButton = this.add.text(100, 400, "Create Room", {fontSize: "32px", color: "#fff"});
        this.createRoomButton.setInteractive();
        this.createRoomButton.on("pointerdown", () => {
            socket.emit("create-room");
        });
        // ルーム作成失敗時のイベント
        socket.on('room-create-error', (errorMessage) => {
            alert(errorMessage);
        });

        // ルームに参加ボタン
        this.joinRoomButton = this.add.text(100, 500, "Join Room", {fontSize: "32px", color: "#fff"});
        this.joinRoomButton.setInteractive();
        this.joinRoomButton.on("pointerdown", () => {
            // ルームIDを入力させるなどの処理を行い、適切なルームIDを取得する必要があります
            const roomId = prompt("Enter Room ID:");
            if (roomId) {
                socket.emit("join-room", roomId);
            }
        });
        // ルーム参加失敗時のイベント
        socket.on('join-room-error', (errorMessage) => {
            alert(errorMessage);
        });
        socket.on('room-not-found', () => {
            alert("Room not found");
        });

        // ルーム退出ボタン
        this.leaveRoomButton = this.add.text(100, 600, "Leave Room", {fontSize: "32px", color: "#fff"});
        this.leaveRoomButton.setInteractive();
        this.leaveRoomButton.on("pointerdown", () => {
            socket.emit("leave-room");
            this.roomIdText.setText("Room ID: ");
        });

        //ルームID表示
        this.roomIdText = this.add.text(100, 200, "Room ID: ", { fontSize: "24px", color: "#fff" });

        socket.on('room-created', (roomId: string) => {
            this.roomIdText.setText(`Room ID: ${roomId}`);
        });

        socket.on('room-joined', (roomId: string) => {
            this.roomIdText.setText(`Room ID: ${roomId}`);
        });

        // ルーム一覧を取得
        socket.emit('get-room-list');

        // ルーム一覧を取得して表示
        this.getRoomListAndDisplay();

        // 1秒ごとにルーム一覧を取得して表示を更新
        this.time.addEvent({
            delay: 1000, // 1秒ごとに
            loop: true,
            callback: this.getRoomListAndDisplay,
            callbackScope: this
        });

    }

    private getRoomListAndDisplay() {
        socket.emit('get-room-list'); // サーバーにルーム一覧を要求
        socket.once('room-list', (roomList) => { // 一度だけルーム一覧を受信するイベントリスナーを設定
            console.log('Room List:', roomList); // ルーム一覧をコンソールに表示
            this.displayRoomList(roomList); // ルーム一覧を表示
        });
    }

    private displayRoomList(roomList: { roomId: string; users: number;}) {
        this.add.text(600, 100, "Room List", { fontSize: "32px", color: "#fff" });
        if (!roomList || roomList.length === 0) {
            this.roomListTexts.forEach(text => text.destroy());
            this.roomListTexts = [];
            console.log("Room list is empty.");
            return;
        }
        // 一旦現在のルーム一覧のテキストをクリア
        this.roomListTexts.forEach(text => text.destroy());
        this.roomListTexts = [];

        let yPos = 200; // ルーム一覧の表示位置
        roomList.forEach(room => {
            const roomText = this.add.text(600, yPos, `Room ID: ${room.roomId}, Users: ${room.users}`, { fontSize: "24px", color: "#fff" });
            this.roomListTexts.push(roomText); // テキストを配列に追加
            yPos += 30; // 次のルームIDの表示位置を調整
        });
    }
}